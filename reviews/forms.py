#THE CHECK LIST
#creat or change the data model if needed
#if model was created or changed make and run migrations
#create or change form for data model if we need a form
#create or change html template for rendering
#create or change a view function
#create a url path config if we create a view function
from django.forms import ModelForm

from reviews.models import Review


class ReviewForm(ModelForm):
    class Meta:
        model = Review
        fields = "__all__"


#from django.forms import ModelForm

#from reviews.models import Review

#class ReviewForm(ModelForm):
#    class Meta:
#        model = Review
#        Fields = "__all__"
