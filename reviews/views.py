from django.shortcuts import render, redirect
from django.shortcuts import render, redirect

# Create your views here.
from reviews.models import Review
from reviews.forms import ReviewForm

def review_detail(request, id):
    review= Review.objects.get(id=id)
    context = {
        "review": review
    }
    return render(request, "detail.html", context)

#from reviews.models import Review
#from reviews.forms import ReviewForm
def create_review(request):
    form = ReviewForm()
    if request.method == "POST":
        form = ReviewForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("reviews_list")
    context = {
        "form": form,
    }
    return render(request, "create.html", context)


#def create_review(request):                               # declareing the function name with single input perameter
#    form = ReviewForm()                                   # create a new review  form inported from the forms model
#    if request.method == "POST":                          # if request is a poast request then
#        form = ReviewForm(request.POST)                   # creat a new form with the data in the http poast
#        if form.is_valid():                               # test to see if data is valid
#            form.save()                                   # since data is valid save the data
#            return redirect("reviews_list")               #redirect the user back to the main page where the list is kept(nothing eles will run after this if both if statments are true)
#    context = {                                           # create a context dictionary
#        "form":form,                                      # add the form to the dictionary
#    }                                                     # close curly brackets
#    return render(request, "reviews/create.html", context)#render the template and return that to dj


def list_reviews(request):
    reviews = Review.objects.all()
    context = {
        "reviews": reviews,
    }
    return render(request, "main.html", context)


#from reviews.models import Review                         #import review modle so we can get reviews from the data base


#def list_reviews(request):                                # this function grabs all of the reviews from the data base to be despalaed this is done usingobjects.all() comand
#    reviews = Review.objects.all()                        #this data is then stored in theis context dictonary
#    context = {
#        "reviews": reviews,
#    }
#    return render(request, "reviews/main.html", context)


    # BASE VIEW FUNCTION BONES
    # frm tweets.models import tweets

    #def list_tweet(request):
        #tweets = tweet.objects.all()
        #context = [
        # "tweets", tweets,
        # ]
        #return render(request, "tweets/main.html", context)